# smolprompt
a small and simple prompt for zsh

```sh
export PROMPT="%F{blue}(%1d) %F{magenta}\$ %F{reset}"
```

![](smolprompt.png)
